package com.example.lenovo.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private ImageButton imgbtnclose;
    private TextView tvresult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        this.tvresult = (TextView) findViewById(R.id.tv_result);
        this.imgbtnclose = (ImageButton) findViewById(R.id.imgbtnclose);

        imgbtnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close();
            }
        });
        int losses=this.getIntent().getIntExtra("losses",0);//Player B Losses(Player A wins)
        int wins=this.getIntent().getIntExtra("wins",0);//Player B Wins(Player A Losses)
        int draws=this.getIntent().getIntExtra("draws",0);//Draws

        tvresult.setText("Player A wins "+losses+"\nPlayer B wins "+wins+"\nTie : "+draws);
    }

    private void close(){
        this.finish();
    }

}
