package com.example.lenovo.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int wins,draws,losses=0;
    int num_current_round=1;

    //nbr of total rounds, 100 in this case.
    int last_round=10;

    private ImageButton imgbtnclose;
    private ImageButton imgbtnreset;

    private TextView tvnumround;
    private Button btnplay;
    private TextView tvinforesult;
    private ImageView ivplayerB;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.ivplayerB = (ImageView) findViewById(R.id.iv_playerB);
        this.tvinforesult = (TextView) findViewById(R.id.tv_info_result);
        this.btnplay = (Button) findViewById(R.id.btnplay);

        this.tvnumround = (TextView) findViewById(R.id.tv_num_round);
        this.imgbtnclose = (ImageButton) findViewById(R.id.imgbtnclose);
        this.imgbtnreset=(ImageButton) findViewById(R.id.imgbtnreset);


        tvnumround.setText("Round "+num_current_round+" / "+last_round);

        imgbtnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close();
            }
        });


        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play();
            }
        });
        imgbtnreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //reset all variables to initial values
                wins=0;
                draws=0;
                losses=0;
                num_current_round=1;
                tvinforesult.setText("");
                btnplay.setText("PLAY");
                tvnumround.setText("Round "+num_current_round+" / "+last_round);
                ivplayerB.setImageDrawable(null);

            }
        });
    }

    private void close(){
        this.finish();
    }

    private void play(){


        //if(num_current_round<=last_round) {
            //random recieve numbers from 0 to 2 (0 : Rock ; 1 : Scissors ; 2 : Paper
            int random = new Random().nextInt(3);
            switch (random) {
                case 0: //case rock
                    ivplayerB.setImageResource(R.drawable.rock);
                    tvinforesult.setText("Player A wins "+" the round "+num_current_round);

                    //nbr losses of player B increment
                    losses = losses + 1;
                    break;
                case 1:
                    ivplayerB.setImageResource(R.drawable.scissors);
                    tvinforesult.setText("Player B wins "+" the round "+num_current_round);

                    //nbr wins of player B increment
                    wins = wins + 1;
                    break;
                case 2:
                    ivplayerB.setImageResource(R.drawable.paper);
                    tvinforesult.setText("Round "+num_current_round+" : Draw.");

                    //nbr draws of player B increment
                    draws = draws + 1;
                    break;
            }
            num_current_round=num_current_round+1;
            //check when current round is the last one (we dont wish to show 101)
            if(num_current_round<=last_round) {
                tvnumround.setText("Round " + num_current_round + " / " + last_round);
            }

            //after last round is played
            if (num_current_round==last_round+1)
            {
                btnplay.setText("Show Results");
                //button play show another activity  or an alert with the final result.
                btnplay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //show result
                        Intent intent=new Intent(MainActivity.this,ResultActivity.class);
                        intent.putExtra("losses",losses);
                        intent.putExtra("wins",wins);
                        intent.putExtra("draws",draws);

                       //reset all values to their initial values after result was showen
                        wins=0;
                        draws=0;
                        losses=0;
                        num_current_round=1;
                        tvinforesult.setText("");
                        btnplay.setText("PLAY");
                        tvnumround.setText("Round "+num_current_round+" / "+last_round );
                        ivplayerB.setImageDrawable(null);


                        startActivity(intent);


                    }
                });
            }

        //}


    }
}
